const express = require('express')
const bodyParser = require('body-parser')

const port = process.env.PORT || 3000
const db = require('./src/database').connection
const app = express()
    
db.once('open', function(){
    // [BEGIN app-config-n-middlewares]
    const cors = require('./src/middlewares/cors.js')
    app.use(cors.config);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    // [END app-config-n-middlewares]
    
    // [BEGIN routes-importing]
    require('./src/controllers/libraryController')(app)
    require('./src/controllers/bookController')(app)
    // [END routes-importing]

    // Start the server
    app.listen(port, () => {
        console.log('Running on port:' + port)
    });
})
