const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Library = require('../models/libraryModel.js')
const Book = require('../models/bookModel.js')

router.post('/', (req, res) => {
    Library.create(req.body)
    .then(doc => {
        return res.status(201).send(doc)
    })
    .catch(err => {
        console.log(err)
        return res.status(400).send(err)
    })
})

router.get('/', async (req, res) => {
    try {
        const name = req.query.name || null, 
            country = req.query.country || null,
            state = req.query.state || null,
            city = req.query.city || null
        
        const query = { }
        if (name) query.name = name
        if (country) query.country = country
        if (state) query.state = state
        if (city) query.city = city

        const libraries = await Library.find(query)
        return res.send(libraries)
    } catch (error) {
        console.log(error)
        return res.status(400).send(error)
    }
})

router.use('/:library_id', async (req, res, next) => {
    const library_id = req.params.library_id

    try {
        const library = await Library.findById(library_id).populate('books')
        req.library = library
        return next()
    } catch (error) {
        console.log(error)
        return res.status(404).send(error)
    }
})
router.get('/:library_id', async (req, res) => {
    return res.send(req.library)
})

router.patch('/:library_id', async (req, res) => {
    const library = req.library

    for (const key in req.body) {
        if (key == '_id') continue
        if (key == 'books') continue
        library[key] = req.body[key]
    }

    try {
        await library.save()
        return res.send(library)
    } catch (error) {
        console.log(error)
        return res.status(400).send(error)
    }
})

router.delete('/:library_id', async (req, res) => {
    try {
        const library_id = req.params.library_id
        
        // Start transaction
        const session = await mongoose.startSession()
        session.startTransaction()

        // Delete library first
        Library.findOneAndDelete({ _id: library_id }, { session })
        .then(async library => {
            const books = library.books

            // After library is deleted, deletes all books
            Book.deleteMany({ _id: { $in: books } }, { session }) 
            .then(async () => {
                await session.commitTransaction()
                return res.status(204).send()
            })
        })
        .catch(async err => {
            await session.abortTransaction()
            console.log(err)
            return res.status(400).send(err)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).send(error)
    }
})

module.exports = app => app.use('/library', router);
