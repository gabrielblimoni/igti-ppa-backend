const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Book = require('../models/bookModel.js')
const Library = require('../models/libraryModel.js')

// You can only add books into libraries
router.post('/:library_id', async (req, res) => {
    try {
        const library_id = req.params.library_id
        
        // Start transaction
        const session = await mongoose.startSession()
        session.startTransaction()

        req.body.libraryId = library_id

        // Create book first
        Book.create([req.body], { session })
        .then(async book => {
            const new_book_id = book[0]._id

            // After book is created, pushes it to the current library
            Library.updateOne({ _id: library_id }, { $push: { books: new_book_id } }, { session }) 
            .then(async () => {
                await session.commitTransaction()
                return res.status(201).send(book)
            })
        })
        .catch(async err => {
            await session.abortTransaction()
            console.log(err)
            return res.status(400).send(err)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).send(error)
    }
})

router.use('/:book_id', async (req, res, next) => {
    const book_id = req.params.book_id

    try {
        const book = await Book.findById(book_id).populate('libraryId')
        req.book = book
        return next()
    } catch (error) {
        console.log(error)
        return res.status(404).send(error)
    }
})
router.get('/:book_id', async (req, res) => {
    return res.send(req.book)
})

router.patch('/:book_id', async (req, res) => {
    const book = req.book

    for (const key in req.body) {
        if (key == '_id') continue
        if (key == 'library_id') continue
        book[key] = req.body[key]
    }

    try {
        await book.save()
        return res.send(book)
    } catch (error) {
        console.log(error)
        return res.status(400).send(error)
    }
})

router.delete('/:book_id', async (req, res) => {
    try {
        const book_id = req.params.book_id
        
        // Start transaction
        const session = await mongoose.startSession()
        session.startTransaction()

        // Delete book first
        Book.findOneAndDelete({ _id: book_id }, { session })
        .then(async book => {
            const library_id = book.libraryId

            // After book is deleted, removes it from the current library
            Library.updateOne({ _id: library_id }, { $pull: { books: book._id } }, { session }) 
            .then(async () => {
                await session.commitTransaction()
                return res.status(204).send()
            })
        })
        .catch(async err => {
            await session.abortTransaction()
            console.log(err)
            return res.status(400).send(err)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).send(error)
    }
})

module.exports = app => app.use('/book', router);
