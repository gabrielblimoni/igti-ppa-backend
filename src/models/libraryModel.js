const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

const LibrarySchema = new mongoose.Schema({
  name: { type: String, required: true },
  country: { type: String, required: true },
  state: { type: String, required: true },
  city: { type: String, required: true },
  books: [ { type: ObjectId, ref: 'Book' } ],
  createdAt: { type: Date, default: Date.now },
})

const Library = mongoose.model('Library', LibrarySchema)

module.exports = Library
