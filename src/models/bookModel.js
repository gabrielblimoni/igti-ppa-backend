const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

const BookSchema = new mongoose.Schema({
  name: { type: String, required: true },
  author: { type: String, required: true },
  year: { type: Number, required: true },
  edition: { type: String, required: true },
  libraryId: { type: ObjectId, required: true, ref: 'Library' },
  createdAt: { type: Date, default: Date.now },
})

const Book = mongoose.model('Book', BookSchema)

module.exports = Book
